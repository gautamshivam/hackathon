﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {

            List<Student> students = new List<Student>();

            Student student1 = new Student();
            student1.Name = "Babnish";
            student1.ScholarNo = 123565;
            Student student2 = new Student();
            student2.Name = "saurabh";
            student2.ScholarNo = 123565;
            Student student3 = new Student();
            student3.Name = "abhishek";
            student3.ScholarNo = 123565;

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            return View(students);
        }

        public ActionResult Add(Student studentSubmitModel)
        {
            List<Student> students = new List<Student>();

            Student student1 = new Student();
            student1.Name = "Babnish";
            student1.ScholarNo = 123565;
            Student student2 = new Student();
            student2.Name = "saurabh";
            student2.ScholarNo = 123565;
            Student student3 = new Student();
            student3.Name = "abhishek";
            student3.ScholarNo = 123565;

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            students.Add(studentSubmitModel);

            return View("Index", students);


        }

        public ActionResult Details(int id)
        {
            List<Student> students = new List<Student>();

            Student student1 = new Student();
            student1.Name = "Babnish";
            student1.ScholarNo = 123565;
            Student student2 = new Student();
            student2.Name = "saurabh";
            student2.ScholarNo = 123565;
            Student student3 = new Student();
            student3.Name = "abhishek";
            student3.ScholarNo = 123565;

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            Student selectedStudent = new Student();
            selectedStudent = students.Select(item => item).Where(student => student.ScholarNo == id).FirstOrDefault();

            if (selectedStudent == null)
            {
                return View();
            }
            else
            {
                return View(selectedStudent);
            }
            
        }

        public ActionResult Edit(int id)
        {
            return View();
        }

        public ActionResult Delete(int id)
        {
            List<Student> students = new List<Student>();

            Student student1 = new Student();
            student1.Name = "Babnish";
            student1.ScholarNo = 123565;
            Student student2 = new Student();
            student2.Name = "saurabh";
            student2.ScholarNo = 123565;
            Student student3 = new Student();
            student3.Name = "abhishek";
            student3.ScholarNo = 123565;

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);


            Student selectedStudent = new Student();
            selectedStudent = students.Select(item => item).Where(student => student.ScholarNo == id).FirstOrDefault();

            if (selectedStudent == null)
            {
                return View("Index",students);

            }
            else
            {
                students.Remove(selectedStudent);
                return new RedirectResult("Index");
            }
        }
    }
}