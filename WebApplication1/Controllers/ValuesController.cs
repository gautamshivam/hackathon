﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/values")]
    public class ValuesController : ApiController
    {
        [HttpGet]
        [Route("api/values/getstudents/{id}")]
        public IHttpActionResult GetStudents(int id)
        {
            List<Student> students = new List<Student>();

            Student student1 = new Student();
            student1.Name = "Babnish";
            student1.ScholarNo = 123565;
            Student student2 = new Student();
            student2.Name = "saurabh";
            student2.ScholarNo = 123565;
            Student student3 = new Student();
            student3.Name = "abhishek";
            student3.ScholarNo = 123565;

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            if(id == student1.ScholarNo)
            {
                return Content(HttpStatusCode.OK, student1);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest,"student with this id not found" );
            }
           
        }

        [HttpPost]
        [Route("api/values/addstudent")]
        public IHttpActionResult AddStudent(Student studentSubmitModel)
        {
            List<Student> students = new List<Student>();

            Student student1 = new Student();
            student1.Name = "Babnish";
            student1.ScholarNo = 1;
            Student student2 = new Student();
            student2.Name = "saurabh";
            student2.ScholarNo = 2;
            Student student3 = new Student();
            student3.Name = "abhishek";
            student3.ScholarNo = 3;

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            Student selectedStudent = new Student();
            selectedStudent = students.Select(item => item).Where(student => student.ScholarNo == studentSubmitModel.ScholarNo).FirstOrDefault();

            if(selectedStudent == null)
            {
                students.Add(studentSubmitModel);
                return Content(HttpStatusCode.Created, students);
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, "student already exists");
            }
            
        }

        [HttpPut]
        [Route("api/values/updatestudent")]
        public IHttpActionResult UpdateStudent(Student studentUpdateModel)
        {
            List<Student> students = new List<Student>();

            Student student1 = new Student();
            student1.Name = "Babnish";
            student1.ScholarNo = 1;
            Student student2 = new Student();
            student2.Name = "saurabh";
            student2.ScholarNo = 2;
            Student student3 = new Student();
            student3.Name = "abhishek";
            student3.ScholarNo = 3;

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);



            Student selectedStudent = new Student();
            selectedStudent = students.Select(item => item).Where(student => student.ScholarNo == studentUpdateModel.ScholarNo).FirstOrDefault();

            if (selectedStudent == null)
            {
                return Content(HttpStatusCode.BadRequest, "student does not exists");
                
            }
            else
            {
                students.Remove(selectedStudent);
                students.Add(studentUpdateModel);
                return Content(HttpStatusCode.Created, students);
            }

        }

        [HttpDelete]
        [Route("api/values/deletestudent/{id}")]
        public IHttpActionResult DeleteStudent(int id)
        {
            List<Student> students = new List<Student>();

            Student student1 = new Student();
            student1.Name = "Babnish";
            student1.ScholarNo = 1;
            Student student2 = new Student();
            student2.Name = "saurabh";
            student2.ScholarNo = 2;
            Student student3 = new Student();
            student3.Name = "abhishek";
            student3.ScholarNo = 3;

            students.Add(student1);
            students.Add(student2);
            students.Add(student3);


            Student selectedStudent = new Student();
            selectedStudent = students.Select(item => item).Where(student => student.ScholarNo == id).FirstOrDefault();

            if (selectedStudent == null)
            {
                return Content(HttpStatusCode.BadRequest, "student does not exists");

            }
            else
            {
                students.Remove(selectedStudent);
                return Content(HttpStatusCode.Created, students);
            }
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }

  
}
